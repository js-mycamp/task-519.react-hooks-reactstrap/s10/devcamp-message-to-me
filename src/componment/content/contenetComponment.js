
import {  useState } from 'react';
import Input from './input/inputContent'
import Output from './output/outputContent'

function Content  () {
    const [inputMessage,setInputMessage] = useState("")
    const [outputMessage,setOutputMessage] = useState([])
    const [likeDisplay,setLikeDisplay] = useState(false)
   
  

  const  inputMessageChangeHandler = (value) =>{
    setInputMessage(value)
    } 
    const outputMessageChangeHandler = () => {
        if(inputMessage){
            setOutputMessage([...outputMessage, inputMessage])
            setLikeDisplay(true)
          
        }
    }

        return (
            <>
                <Input inputMessageProps={inputMessage} inputMessageChangeHandlerProps={inputMessageChangeHandler} outputMessageChangeHandlerProps={outputMessageChangeHandler}/>
                <Output outputMessageProp={outputMessage} likeDisplayProps={likeDisplay} />
            </>
    
        )
    
}
export default Content