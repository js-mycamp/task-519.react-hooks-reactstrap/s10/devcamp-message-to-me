
import imageLike from '../../../assets/images/facebook-like-icon_1017-8081.avif'

function outputContent (props) {
  
        let {outputMessageProp,likeDisplayProps} = props;
        return(
            <>
            
            <div className='col-12 mt-2'>

                {/* nâng cấp*/}
            {outputMessageProp.map((element, index) => {
                    return  <p key={index} >{element} </p>
                })}


                    {/* <p > {outputMessageProp}</p> */}
                </div>
                <div className='row mt-2'>
                    <div className='col-12'>
                        {/* cách 1 */}
                        {/* <img src={imageLike} alt='like' width='100' style={{ display:likeDisplayProps ? "block" : "none"}} />; */}
                         {/* cách 2 */}
                        {likeDisplayProps ?    <img src={imageLike} alt='like' width='100'  /> : <></> }
                    </div>
                </div>
            </>
        )
    
}
export default outputContent